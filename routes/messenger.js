const login = require('facebook-chat-api');
const fs = require('fs');
const scbNodeParser = require('scb-node-parser');
var Message = require('scb-node-parser/message');

var winston = require('winston');

var logger = new(winston.Logger)({
    transports: [
        new(winston.transports.File)({
            filename: 'application.log'
        })
    ]
});

/*Messages look like this:

    {
        "message": "Hi, how are you?",
        "threadID": "facebookID"
    }
*/
var savedState = null;

try {

    savedState = JSON.parse(fs.readFileSync('appstate.json', 'utf8'));
} catch (err) {
    savedState = null;
}
exports.sendMessage = (req, res) => {

    if (savedState === null) {
        login({
            email: process.env.USNB_FACEBOOK_USER,
            password: process.env.USNB_FACEBOOK_PASSWORD
        }, (err, api) => {
            try {
                fs.writeFileSync('appstate.json', JSON.stringify(api.getAppState()));
            } catch (err) {
                logger.log('error', 'authentication_error', err);
            }
            send(err, api, req.body.threadID, req.body.message)
        });

    } else {
        console.log('cached login');
        login({
            appState: savedState
        }, (err, api) => {
            send(err, api, req)
        });
    }


}

exports.sendDirectMessage = (threadID, message) => {
    if (savedState === null) {
        login({
            email: conf.email,
            password: conf.password
        }, function callback(err, api) {
            send(err, api, threadID, message);
        });

    } else {
        console.log('cached login');
        login({
            appState: savedState
        }, (err, api) => {
            api.getUserID(threadID, (err, data) => {
                if (err) return logger.log('error', 'send_error', err);
                var threadID = data[0].userID;
                send(api, threadID, message);
            });
        });
    }

}


function send(api, threadID, message) {
    //if (err) return logger.log('error', 'send_error', err);
    savedState = api.getAppState();
    console.log('sending %s to %s', message, threadID);
    api.sendMessage(message, threadID);
};
