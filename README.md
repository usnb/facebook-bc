# facebook-persona

Configuration: write your Facebook credentials in the conf/conf.js file.


Example:

```
POST /messenger HTTP/1.1
Content-Type: application/json
Host: yourhost

{"threadID":"receiverFacebookID", "message": "hey you! how are you?"}'
```

### License ###

 This software is provided under a dual license model designed to meet the 
 development and distribution needs of both commercial usage and open source 
 projects.
 
 If you intend to use this software for commercial purposes, contact the project
 members presented in the next section.

### Who do I talk to? ###

* Rafael Angarita: rafael.angarita AT inria.fr (main developer)
* Nikolaos Georgantas nikolaos.georgantas AT inria.fr (designer)
* Valérie Issarny valerie.issarny AT inria.fr (designer)