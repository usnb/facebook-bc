#!/usr/bin/env node

/**
 * @file
 * Binding Component implementation automatically generated by
 * the Social Communication Platform.
 *
 * Characteristics:
 * - Type: AMQP subscriber.
 * - Social Communication Platform Bus: AMQP.
 */

var amqp = require('amqplib/callback_api');
var main = require('./main');
var Message = require('scb-node-parser/message');

/**
 * Receives a message from a Social Entity.
 * After receiving a message, it forwards it
 * to the Social Communication Platform Bus.
 * Protocol: AMQP.
 */

exports.listen = () => {

    var connection = process.env.RABBITMQ;
    amqp.connect(connection, (err, conn) => {
        if (err) {
            console.log(err.stack);
        } else {
            //connection error handling
            conn.on('error', (err) => {
                console.log('An error occurred: ' + err.stack);
            });

            conn.createChannel((err, ch) => {
                if (err) {
                    console.log(err.stack);
                } else {
                    connect(ch);
                }
            });
        }
    });
};


function connect(ch) {

    var ex = process.env.USNB_FACEBOOK_PERSONA_EXCHANGE;
    ch.assertExchange(ex, 'fanout', {
        durable: true
    });
    ch.assertQueue(ex, {
        exclusive: false
    }, (err, q) => {
        if (err) {
            console.log(err.stack);
        } else {
            console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
            ch.bindQueue(q.queue, ex, '');

            ch.consume(q.queue, (msg) => {
                var message = JSON.parse(msg.content.toString());
                message.__proto__ = Message.prototype;
                console.log(" [x] %s", JSON.stringify(message));
                main.process(message);
                //TODO: check if the message was sent successfully before doing the ack
                ch.ack(msg);
            });
        }

    }, {
        noAck: false
    });
}
